#--------------------------------------------------------------
#-- SCRIPT: dtools.py v.:0.1 Alpha
#-- DESCRIPTION: Script to create diccitionaris using the word 
#-- a specific web or domain.  Also morph the Dic in differents
#-- ways.
#-- LICENSE: GPL-2.0
#-- ORIGINAL AUTHOR: (madyyelf) Raul Gimenez Herrada
#-- eMAIL: madyyelf@gmail.com
#-- WEB: http://alquimiabinaria.cat
#--------------------------------------------------------------

from BeautifulSoup import BeautifulSoup
import urllib
import nltk
import optparse


def printGPL():
 print '+-------------------------------------------------------------+'
 print '|  SCRIPT: dtools.py v.:0.1 Alpha                             |'
 print '|  LICENSE: GPL-2.0                                           |'
 print '|  ORIGINAL AUTHOR: (madyyelf) Raul Gimenez Herrada           |'
 print '|  WEB: http://alquimiabinaria.cat EMAIL: madyyelf@gmail.com  |'
 print '+-------------------------------------------------------------+'
 print ' '
 print ' '

#First fuction: Create a dictionari with words from a specified domain.
def domain2dic(domain,output_file):
 ctr=1
 url2craw=[]
 dic=[]

 url2craw.append("http://"+domain)
 for url in url2craw:
  cont=True
  newWords=0
  newURLs=0
  print '[+] Processing URL:',url

  try:
   #Getting all links on the domain
   html_page = urllib.urlopen(url)
   soup = BeautifulSoup(html_page)
  except Exception, e:
   print '.. [-] Error downloading the page.'
   cont=False

  if cont==True:
   for link in soup.findAll('a'):
    new_url= link.get('href')
    if new_url:
     if domain in new_url:
      if new_url not in url2craw:
       url2craw.append(new_url)
       newURLs+=1
  
   #Getting all word of the URL
   try:
    html_page = urllib.urlopen(url)
    raw_html=html_page.read()
    words=nltk.clean_html(raw_html).split()
   except Exception, e:
    print '.. [-] Error stripping words.'
    cont=False
   if cont==True:
    for word in words:
     cont2=True
     try:
      #word=str(BeautifulSoup(word,convertEntities=BeautifulSoup.HTML_ENTITIES))
      word.encode('ascii','replace')
     except Exception, a:
      #print '.. [-] Error in escaping HTML codes.'
      cont=False
     if cont2==True:
      word=word.strip()
      word=word.lower()
      word=word.translate(None,'.,!?:()')
      if word not in dic:
       dic.append(word)
       newWords+=1
   
    #Partial results.
    print '.. [+] New words found:',str(newWords),'('+str(len(dic))+') - new URLs Found:',str(newURLs),'('+str(ctr)+'/'+str(len(url2craw))+')'
    ctr+=1
   
 #Printing final results
 print '[+] Generated dictionari of',str(len(dic)),'words!!'
 print '[+] Creating dictionari.'
 file= open(output_file,"w")
 for word in dic:
  file.write(word+'\n')
 file.close()
 
def main():
 parser=optparse.OptionParser('Usage: python dtools.py -d <domain> -of <output file>')
 parser.add_option('-d',dest='i_domain',type='string',help='Domain to craw words.')
 parser.add_option('-o',dest='i_output_file',type='string',help='Output file.')

 (options,args)=parser.parse_args()
 i_domain=options.i_domain
 i_output_file=options.i_output_file
 if i_domain == None or i_output_file == None:
  print parser.usage
  exit(0)
 
 printGPL()
 domain2dic(i_domain,i_output_file)

if __name__=='__main__':
 main()
