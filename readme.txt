#-- SCRIPT: dtools.py v.:0.1 Alpha
#-- DESCRIPTION: Script to create diccitionaris using the word 
#-- a specific web or domain.  Also morph the Dic in differents
#-- ways.
#-- LICENSE: GPL-2.0
#-- ORIGINAL AUTHOR: (madyyelf) Raul Gimenez Herrada
#-- eMAIL: madyyelf@gmail.com
#-- WEB: http://alquimiabinaria.cat


#-- CHANGELOG
16/6/14: Starting project. Basic funcition and pharsing options.
17/6/14: Removing punctuation characters from string, lower-case and blank spaces.  TO DO: Process escaping HTML characters.
20/6/14: Added ASCII encode, basic error handling and GPL-2.0 banners.
